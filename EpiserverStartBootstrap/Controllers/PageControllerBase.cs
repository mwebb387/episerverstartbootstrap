﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using EpiserverStartBootstrap.Models.Pages;

namespace EpiserverStartBootstrap.Controllers
{
    public class PageControllerBase<T> : PageController<T> where T : SitePageData
    {
        // Implement shared controller functionality here...
    }
}