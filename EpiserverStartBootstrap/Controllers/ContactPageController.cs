﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using EpiserverStartBootstrap.Models.Pages;

namespace EpiserverStartBootstrap.Controllers
{
    public class ContactPageController : PageControllerBase<ContactPage>
    {
        public ActionResult Index(ContactPage currentPage)
        {
            /* Implementation of action. You can create your own view model class that you pass to the view or
             * you can pass the page type for simpler templates */

            return View(currentPage);
        }

        [HttpPost]
        public ActionResult Submit(ContactPage currentPage)
        {
            //TODO: Doesn't work. Implement with Block:
            // https://world.episerver.com/blogs/team-oshyn/dates/2016/3/posting-forms-with-episerver-mvc/
            return RedirectToAction("Index");
        }
    }
}