﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EpiserverStartBootstrap.Models.Blocks;

namespace EpiserverStartBootstrap.Controllers
{
    public class BlogPostBlockController : BlockController<BlogPostBlock>
    {
        public override ActionResult Index(BlogPostBlock currentBlock)
        {
            return PartialView(currentBlock);
        }
    }
}
