﻿using System;
using System.Linq;
using System.Web.Optimization;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;

namespace EpiserverStartBootstrap.Business
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class BundleConfig : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            if (context.HostType == HostType.WebApplication)
            {
                RegisterBundles(BundleTable.Bundles);
            }
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        "~/Static/vendor/jquery/jquery.js", //jquery.js can be removed and linked from CDN instead, we use a local one for demo purposes without internet connectionzz
                        "~/Static/vendor/bootstrap/js/bootstrap.js",
                        "~/Static/js/jqBootstrapValidation.js",
                        "~/Static/js/clean-blog.js"));

            bundles.Add(new StyleBundle("~/bundles/css")
                .Include("~/Static/vendor/bootstrap/css/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Static/vendor/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Static/css/clean-blog.css"));
        }

        public void Uninitialize(InitializationEngine context)
        {
            //Add uninitialization logic
        }
    }
}