﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EpiserverStartBootstrap.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EpiserverStartBootstrap.Helpers
{
    public static class HtmlHelpers
    {
        public static IEnumerable<ContentReference> ChildPages(this HtmlHelper helper, ContentReference rootPage)
        {
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            return contentLoader
                .GetChildren<PageData>(rootPage)
                .Where(child => !(child is ContainerPage))
                .Select(child => child.ContentLink);
        }

        public static SettingsPage GetSettingsPage(this HtmlHelper helper)
        {
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            return contentLoader.GetChildren<SettingsPage>(ContentReference.RootPage).FirstOrDefault() ?? new SettingsPage();
        }
    }
}