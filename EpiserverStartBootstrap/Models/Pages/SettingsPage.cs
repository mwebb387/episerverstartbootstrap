﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;

namespace EpiserverStartBootstrap.Models.Pages
{
    [ContentType(DisplayName = "SettingsPage", GUID = "1625c86f-a0dc-48ed-a011-a2c4648a2045", Description = "")]
    public class SettingsPage : PageData
    {
        [CultureSpecific]
        [Display(
            Name = "Site Name",
            Description = "The name of the site",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string SiteName { get; set; }

        //Other global settings go here...
        // Reference:
        // http://jondjones.com/learn-episerver-cms/episerver-developers-guide/episerver-structuring-your-project/the-best-way-to-manage-global-data-within-episerver
    }
}