﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;

namespace EpiserverStartBootstrap.Models.Pages
{
    [ContentType(DisplayName = "PostPage", GUID = "4c1c8ba2-d205-4b25-ac56-b4b857319d4f", Description = "")]
    public class BlogPostPage : SitePageData
    {
        [CultureSpecific]
        [Display(
            Name = "Author",
            Description = "The name of the post author",
            GroupName = SystemTabNames.Content,
            Order = 310)]
        public virtual string Author { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Post Time",
            Description = "The time of the post",
            GroupName = SystemTabNames.Content,
            Order = 320)]
        public virtual DateTime PostTime { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 330)]
        public virtual XhtmlString MainBody { get; set; }
    }
}