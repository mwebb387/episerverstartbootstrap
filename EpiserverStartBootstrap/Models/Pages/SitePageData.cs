﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using EPiServer;

namespace EpiserverStartBootstrap.Models.Pages
{
    [ContentType(DisplayName = "SitePageData", GUID = "9af291d9-797a-4fae-bf38-a1bea4e75194", Description = "")]
    public class SitePageData : PageData
    {
        [CultureSpecific]
        [Display(
            Name = "Title",
            Description = "The title of the page",
            GroupName = SystemTabNames.Content,
            Order = 100)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Header",
            Description = "The header of the page",
            GroupName = SystemTabNames.Content,
            Order = 100)]
        [UIHint(UIHint.Textarea)]
        public virtual string Header { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Sub Header",
            Description = "The sub header of the page",
            GroupName = SystemTabNames.Content,
            Order = 200)]
        [UIHint(UIHint.Textarea)]
        public virtual string SubHeader { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Header Image",
            Description = "The header image",
            GroupName = SystemTabNames.Content,
            Order = 300)]
        [UIHint(UIHint.Image)]
        public virtual Url HeaderImageUrl { get; set; }
    }
}