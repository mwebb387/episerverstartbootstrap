﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EpiserverStartBootstrap.Models.Blocks;

namespace EpiserverStartBootstrap.Models.Pages
{
    [ContentType(DisplayName = "StartPage", GUID = "106d75ba-eb54-4524-9f27-55e29204a585", Description = "")]
    public class StartPage : SitePageData
    {
        //[CultureSpecific]
        //[Display(
        //    Name = "Main body",
        //    Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
        //    GroupName = SystemTabNames.Content,
        //    Order = 1)]
        //public virtual XhtmlString MainBody { get; set; }

        [Display(
            Name = "Post List",
            Description = "The list of posts",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual ContentArea PostList { get; set; }
    }
}