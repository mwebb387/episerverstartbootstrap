﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;

namespace EpiserverStartBootstrap.Models.Pages
{
    //TODO: Renderng in the tree-view. Possible reference:
    // http://world.episerver.com/blogs/linus-ekstrom/dates/2011/3/container-pages/

    [ContentType(DisplayName = "ContainerPage", GUID = "743e264a-99cc-451e-b8b6-07f450966d5f")]
    public class ContainerPage : SitePageData
    {
    }
}