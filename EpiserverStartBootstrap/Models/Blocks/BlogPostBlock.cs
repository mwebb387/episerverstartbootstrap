﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace EpiserverStartBootstrap.Models.Blocks
{
    [ContentType(DisplayName = "BlogPostBlock", GUID = "8486a8ad-7b9a-4fa0-951d-a2f67766b82a", Description = "")]
    public class BlogPostBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "Title",
            Description = "Blog post title",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Title { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Subtitle",
            Description = "Blog post subtitle",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Subtitle { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Author",
            Description = "The name of the post author",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Author { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Post Time",
            Description = "The time of the post",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual DateTime PostTime { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Post",
            Description = "The blog page post",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual PageReference Post { get; set; }
    }
}